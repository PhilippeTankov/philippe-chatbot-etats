import sys, logging, requests, time, math

bot_token = sys.argv[1]

import logging

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# GENDER, PHOTO, LOCATION, BIO = range(4) == Les etats
# Création des etats
START, RESTAURANT_CHOIX, RESTAU_RESULTATS, RESTAU_DETAILS, SORTIR_CHOIX, SORTIR_DETAILS, SORTIR_RECOMMENDATIONS, TRANSPORT = range(8)

#Méthode START qui propose un choix entre sortir ou aller au restaurant, et qui renvoie sur l'état restaurant_choix
def start(update, context):
    reply_keyboard = [['Sortir', 'Aller au Restaurant', 'Transport']]
    user = update.message.from_user

    update.message.reply_text('Salut😜 ! Je suis un 🤖 chatbot ayant pour but d\'aider les utilisateurs à trouver quoi faire.. Cool, non?\n '
                              'C\'est parti 👌! Tu souhaites plutôt... \n'
                              'Pour arrêter de me parler, envoie /cancel. \n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return START


########################################################################################################################

                        # CHOIX DE RESTAURANT + RESULTATS + DETAILS SELON LA CUISINE CHOISIE

########################################################################################################################
def restaurant_choix(update, context):
    #reply_keyboard = [['Chinoise', 'Française', 'Indienne', 'Italienne', 'Japonaise', 'Retour']]
    reply_keyboard = [['🇨🇳 Chinoise', '🇫🇷 Française', '🇮🇳 Indienne', '🇮🇹 Italienne', '🇯🇵 Japonnaise', 'Retour']]
    user = update.message.from_user
    logger.info("User %s: a choisit %s", user.first_name, update.message.text)

    update.message.reply_text('Ha! Toi aussi tu as faim? 🍲\n'
                              'Je vais te suggérer les meilleurs restaurants du coin 🍽\n\n'
                              'Quel type de Cuisine aimerais-tu?\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT_CHOIX


# Boucle contenant les choix de réponse
def restau_resultats(update, context):
    user = update.message.from_user
    rep_user = update.message.text
    logger.info("User %s: a choisit %s", user.first_name, update.message.text)

    if rep_user in ['Chinoise', 'Chinoise']:
        return chine(update, context)
    elif rep_user in ['Française','Française']:
        return france(update, context)
    elif rep_user in ['indienne','indienne']:
        return inde(update, context)
    elif rep_user in ['italienne', 'italienne']:
        return italie(update, context)
    elif rep_user in ['japonaise','japonaise']:
        return japon(update, context)
    elif rep_user in ['Retour', 'retour']:
        return start(update, context)

    return ConversationHandler.END

def chine(update, context):
    #reply_keyboard = [['Merci pour la recommendation, bye !']]
    reply_keyboard = [['Restaurant Hungky'], ['Restaurant BOKY'], ['Chez Sunny'], ['Retour']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('🇨🇳 De la cuisine Chinoise 🇨🇳 \n'
                              'Super choix, voici mes recommendations🐉 : \n\n'
                              '- Restaurant Hungky\n'
                              '- Restaurant BOKY\n'
                              '- Chez Sunny\n\n'
                              'Fun fact 🉐: Lors de visites amicales, les Chinois offrent des fruits plutôt que des fleurs.',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
                              #reply_markup=ReplyKeyboardRemove(keyboard=[]))

    return RESTAU_DETAILS

def france(update, context):
    reply_keyboard = [['Café de Paris'], ['Café des Amis'], ['Café du Levant'], ['Retour']]
    #reply_keyboard = [['Merci pour la recommendation, bye !']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('🇫🇷 De la cuisine Française 🇫🇷 \n'
                              'Super choix, voici mes recommendations 🧀: \n'
                              '- Le Café de Paris\n'
                              '- Le Café des Amis\n'
                              '- Le Café du Levant\n\n'
                              'Fun fact 🥖: Louis XIX fut le roi de France avec le règne le plus court, 20 minutes..',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAU_DETAILS

def inde(update, context):
    reply_keyboard = [['RAJPOUTE'], ['Sajna'], ['Bombay Palace'], ['Retour']]
    #reply_keyboard = [['Merci pour la recommendation, bye !']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('🇮🇳 De la cuisine Indienne 🇮🇳 \n'
                              'Super choix, voici mes recommendations 🧞: \n'
                              '- Le RAJPOUTE\n'
                              '- Le Sajna\n'
                              '- Le Bombay Palace\n\n'
                              'Fun fact 🐘: Plus de la moitié des Indiens ont moins de 25 ans.',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAU_DETAILS


def italie(update, context):
    reply_keyboard = [['La MATZE'], ['La Gioconda'], ['Tosca'], ['Retour']]
    #reply_keyboard = [['Merci pour la recommendation, bye !']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('🇮🇹 De la cuisine Italienne 🇮🇹 \n'
                              'Super choix, voici mes recommendations 🍕 :\n'
                              '- La MATZE\n'
                              '- La Gioconda\n'
                              '- Tosca\n\n'
                              'Fun fact 🍅: Le nom "Italie" vient du grec "Italos" qui signifie veau',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAU_DETAILS


def japon(update, context):
    reply_keyboard = [['Mikado Sushi'], ['Restaurant Ajinokura'], ['Shogun'], ['Retour']]
    #reply_keyboard = [['Merci pour la recommendation, bye !']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('🇯🇵 De la cuisine Japonaise 🇯🇵 \n'
                              'Super choix, voici mes recommendations 🎎: \n'
                              '- Le Mikado Sushi\n'
                              '- Le Restaurant Ajinokura\n'
                              '- Le Shogun\n\n'
                              'Fun fact 🏯: Au restaurant, l\'eau, le thé et des serviettes chaudes sont toujours offerts au Japon',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAU_DETAILS



def restau_details (bot, update):
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    #Chinoise
    if rep_user in ['Restaurant Hungky', 'Restaurant Hungky']:
        update.message.reply.location(46.2092072,6.1444671)
    elif rep_user in ['Restaurant BOKY', 'Restaurant BOKY']:
        update.message.reply.location(46.209898,6.1450945)
    elif rep_user in ['Chez Sunny', 'Chez Sunny']:
        update.message.reply.location(48.862725,2.287592)
    elif rep_user in ['Retour', 'Retour']:
        return start(update, context)

    #Francais
    if rep_user in ['Café de Paris', 'Café de Paris']:
        update.message.reply.location(46.2092074,6.143795)
    elif rep_user in ['Café des Amis', 'Café des Amis']:
        update.message.reply.location(46.2238839,6.225276)
    elif rep_user in ['Café du Levant', 'Café du Levant']:
        update.message.reply.location(46.1908,6.0454)
    elif rep_user in ['Retour', 'Retour']:
        return start(update, context)

    #Indie
    if rep_user in ['RAJPOUTE', 'RAJPOUTE']:
        update.message.reply.location(46.1899234,6.1435749)
    elif rep_user in ['Sajna', 'Sajna']:
        update.message.reply.location(46.2110346,6.14594645)
    elif rep_user in ['Bombay Palace', 'Bombay Palace']:
        update.message.reply.location(46.2055535,6.1608817)
    elif rep_user in ['Retour', 'Retour']:
        return start(update, context)


    #Ita
    if rep_user in ['La MATZE', 'La MATZE']:
        update.message.reply.location(46.20955,6.1447787)
    elif rep_user in ['La Gioconda', 'La Gioconda']:
        update.message.reply.location(46.2256565,6.1059398)
    elif rep_user in ['Tosca', 'Tosca']:
        update.message.reply.location(46.2026198,6.1584308)
    elif rep_user in ['Retour', 'Retour']:
        return start(update, context)


    #Jap
    if rep_user in ['Mikado Sushi', 'Mikado Sushi']:
        update.message.reply.location(48.862725,2.287592)
    elif rep_user in ['Restaurant Ajinokura', 'Restaurant Ajinokura']:
        update.message.reply.location(46.2066027,6.140304)
    elif rep_user in ['Shogun', 'Shogun']:
        update.message.reply.location(46.2041592,6.1588969)
    elif rep_user in ['Retour', 'Retour']:
        return start(update, context)

    return ConversationHandler.END

########################################################################################################################

                        # CHOIX DE SORTIE + RECOMMENDATIONS DE SORTIES + DETAILS SELON LA SORTIE CHOISIE

########################################################################################################################

def sortir_choix(update, context):
    reply_keyboard = [['🏛 Musées', '🍻 Bars', '🎉 Clubs', 'Retour']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('Bon choix! 🤖 Je connais de bons endroits..\n '
                              'Que souhaites-tu faire?',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return SORTIR_RECOMMENDATIONS

#Boucle contenant les réponses aux choix
def sortir_recommendations(update, context):
    user = update.message.from_user
    rep_user = update.message.text
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    if rep_user in ['Musées', 'museums']:
        return museums(update, context)
    elif rep_user in ['Bars','bars']:
        return bars(update, context)
    elif rep_user in ['Clubs','clubs']:
        return clubs(update, context)
    elif rep_user in ['Retour', 'retour']:
        return start(update, context)

    return ConversationHandler.END

def museums(update, context):
    reply_keyboard = [['Musée de Carouge'], ['Musée d\'éthnographie'], ['Musée d\'histoires naturelles'], ['Retour']]
    #reply_keyboard = [['Merci pour la recommendation, bye !']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('🏛 C\'est parti pour les Musées 🏛 \n\n'
                              'Super choix, voici mes recommendations : \n'
                              '- Le musée de Carouge\n'
                              '- Le musée d\'éthnographie\n'
                              '- Le Musée d\'histoires naturelles\n\n'
                              'Fun fact : Le musée du Louvre est si grand que cela nous prendrait 100 jours pour le visiter dans sa totalité en passant 30s dans chaque salle',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return SORTIR_DETAILS
    #return ConversationHandler.END

def bars(update, context):
    reply_keyboard = [['Soleil Rouge'], ['Rooftop 42'], ['le Café de la Pointe'], ['Retour']]
    #reply_keyboard = [['Merci pour la recommendation, bye !']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('🍻 C\'est parti pour les Bars 🍻\n\n'
                              'Super choix, voici mes recommendations : \n'
                              '- Soleil Rouge\n'
                              '- Le Rooftop 42\n'
                              '- le Café de la Pointe\n\n'
                              'Fun fact : En Russie, la bière n\'était pas considérée comme de l\'alcool avant... 2013\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return SORTIR_DETAILS
    #return ConversationHandler.END

def clubs(update, context):
    reply_keyboard = [['ByPASS Geneva' ], ['Petit Palace'], ['VIP Oriental'], ['Retour']]
    #reply_keyboard = [['Merci pour la recommendation, bye !']]
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    update.message.reply_text('🎉 C\'est parti pour les Clubs 🎉\n\n'
                              'Super choix, voici mes recommendations : \n'
                              '- Le ByPASS Geneva\n'
                              '- Le Petit Palace\n'
                              '- Le VIP Oriental\n\n'
                              'Fun fact : Au temps Médiéval, le fait de faire "tchin" était majoritairement utilisé pour éviter de se faire empoisonné.. Sympa l\'ambiance !\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return SORTIR_DETAILS
    #return ConversationHandler.END


def sortir_details (bot, update):
    user = update.message.from_user
    logger.info("User %s: a choisi %s", user.first_name, update.message.text)

    #Musées
    if rep_user in ['Musée de Carouge', 'Musée de Carouge']:
        update.message.reply.location(46.1842913,6.1391515)
    elif rep_user in ['Musée d\'éthnographie', 'Musée d\'éthnographie']:
        update.message.reply.location(46.196266,6.1389645)
    elif rep_user in ['Musée d\'histoires naturelles', 'Musée d\'histoires naturelles']:
        update.message.reply.location(46.1987181,6.1578798)
    elif rep_user in ['Retour', 'Retour']:
        return start(update, context)

    #Clubs
    if rep_user in ['ByPASS Geneva', 'ByPASS Geneva']:
        update.message.reply.location(46.1863746,6.1281714)
    elif rep_user in ['Petit Palace', 'Petit Palace']:
        update.message.reply.location(46.2026782,6.1447321)
    elif rep_user in ['VIP Oriental', 'VIP Oriental']:
        update.message.reply.location(46.2036021,6.149743)
    elif rep_user in ['Retour', 'Retour']:
        return start(update, context)

    #Bars
    if rep_user in ['Soleil Rouge', 'Soleil Rouge']:
        update.message.reply.location(46.2016672,6.1551078)
    elif rep_user in ['Rooftop 42', 'Rooftop 42']:
        update.message.reply.location(46.2041891,6.1470323)
    elif rep_user in ['le Café de la Pointe', 'le Café de la Pointe']:
        update.message.reply.location(46.19513702392578,6.143677234649658)
    elif rep_user in ['Retour', 'Retour']:
        return start(update, context)

    return ConversationHandler.END



########################################################################################################################

                # INTEGRATION DES METHODES DE TRANSPORT (BIENVENUE, LIEU A CHERCHER, COORDONNEES, DETAILS)

########################################################################################################################

def appeler_opendata(path):
   url = "http://transport.opendata.ch/v1/" + path
   reponse = requests.get(url)
   return reponse.json()

# Preparation des messages

def afficher_arrets(update, arrets):
   texte_de_reponse = "Voici les arrets:\n"
   for station in arrets['stations']:
       if station['id'] is not None:
           texte_de_reponse += "\n/a" + station['id'] + " " + station['name']
   update.message.reply_text(texte_de_reponse)

def bienvenue(bot, update):
   update.message.reply_text("Merci d'envoyer votre localisation (via piece jointe ou simplement en texte)")
   #logger.info("Détails de transport")
   update.message.reply_text('Ou êtes-vous ?',
                             reply_markup=ReplyKeyboardRemove())

   return TRANSPORT


def lieu_a_chercher(bot, update):
   resultats_opendata = appeler_opendata("locations?query=" + update.message.text)
   afficher_arrets(update, resultats_opendata)

   return TRANSPORT



def coordonnees_a_traiter(bot, update):
   location = update.message.location
   resultats_opendata = appeler_opendata("locations?x={}&y={}".format(location.latitude, location.longitude))
   afficher_arrets(update, resultats_opendata)

   return TRANSPORT


def details_arret(bot, update):
   arret_id = update.message.text[2:]
   afficher_departs(update, appeler_opendata("stationboard?id=" + arret_id))

   return TRANSPORT


########################################################################################################################

                                     # METHODES DES ERREURS (CANCEL, ERROR)

########################################################################################################################


#Méthode CANCEL qui avertit que l'user veut stopper l'experience
def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! N\'hésites pas à faire appel à mes services une prochaine fois.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END

#Méthode regroupant les erreurs rencontrées pendant l'experience
def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)



def main():
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(bot_token, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Ajout du convHandler avec les différents états créés
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        # Différents états du Bot
        states={

            START: [
                MessageHandler(Filters.regex('^(Sortir)$'), sortir_choix),
                MessageHandler(Filters.regex('^(Aller au Restaurant)$'), restaurant_choix),
                MessageHandler(Filters.regex('^(Transport)$'), bienvenue),
            ],


            RESTAURANT_CHOIX: [
                MessageHandler(Filters.regex('^(🇨🇳 Chinoise)$'), chine),
                MessageHandler(Filters.regex('^(🇫🇷 Française)$'), france),
                MessageHandler(Filters.regex('^(🇮🇳 Indienne)$'), inde),
                MessageHandler(Filters.regex('^(🇮🇹 Italienne)$'), italie),
                MessageHandler(Filters.regex('^(🇯🇵 Japonnaise)$'), japon),
                MessageHandler(Filters.regex('^(Retour)$'), start),

            ],

            RESTAU_DETAILS: [
                MessageHandler(Filters.regex('^(...)$'), restau_details),
                MessageHandler(Filters.regex('^(Retour)$'), start),
            ],

            SORTIR_DETAILS: [
                MessageHandler(Filters.regex('^(...)$'), sortir_details),
                MessageHandler(Filters.regex('^(Retour)$'), start),
            ],

            RESTAU_RESULTATS: [
                MessageHandler(Filters.regex('^(Je veut des détails|Retour aux choix)$'), restau_resultats)
            ],

            SORTIR_CHOIX: [
                MessageHandler(Filters.regex('^(museums|bars|clubs)$'), sortir_recommendations),
            ],

            SORTIR_RECOMMENDATIONS: [
                MessageHandler(Filters.regex('^(🏛 Musées)$'), museums),
                MessageHandler(Filters.regex('^(🍻 Bars)$'), bars),
                MessageHandler(Filters.regex('^(🎉 Clubs)$'), clubs),
                MessageHandler(Filters.regex('^(Retour)$'), start),
            ],

            TRANSPORT: [
                MessageHandler(Filters.text, lieu_a_chercher),
                MessageHandler(Filters.location, coordonnees_a_traiter),
                CommandHandler('detail', details_arret),
            ],
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()